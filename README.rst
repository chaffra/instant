=======
Instant
=======

Instant is a Python module that allows for instant inlining of C and
C++ code in Python. It is a small Python module built on top of SWIG
and Distutils.

Instant is part of the FEniCS Project.

For more information, visit http://www.fenicsproject.org.


Documentation
=============

The Instant documentation can be viewed at Read the Docs:

+--------+-------------------------------------------------------------------------------------+
|Instant |  .. image:: https://readthedocs.org/projects/fenics-instant/badge/?version=latest   |
|        |     :target: http://fenics.readthedocs.io/projects/instant/en/latest/?badge=latest  |
|        |     :alt: Documentation Status                                                      |
+--------+-------------------------------------------------------------------------------------+


License
=======

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU Lesser General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
  GNU Lesser General Public License for more details.

  You should have received a copy of the GNU Lesser General Public License
  along with this program. If not, see <http://www.gnu.org/licenses/>.
